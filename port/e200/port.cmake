set(PORTSRC ${CHIBIOS}/os/common/ports/e200/chcore.c)
set(PORTASM
    ${CHIBIOS}/os/common/ports/e200/compilers/GCC/ivor.S
    ${CHIBIOS}/os/common/ports/e200/compilers/GCC/chcoreasm.S
    )
set(PORTINC
    ${CHIBIOS}/os/common/ports/e200/
    ${CHIBIOS}/os/common/ports/e200/compilers/GCC/
    )

set(ALLXASMSRC ${ALLXASMSRC} ${PORTASM})
set(ALLCSRC ${ALLCSRC} ${PORTSRC})
set(ALLINC ${ALLINC} ${PORTINC})
