set(STARTUPASM
    ${CHIBIOS}/os/common/startup/e200/devices/SPC564Axx/boot.S
    ${CHIBIOS}/os/common/startup/e200/compilers/GCC/vectors.S
    ${CHIBIOS}/os/common/startup/e200/compilers/GCC/crt0.S
    )
set(STARTUPINC
    ${CHIBIOS}/os/common/portability/GCC/
    ${CHIBIOS}/os/common/startup/e200/compilers/GCC/
    ${CHIBIOS}/os/common/startup/e200/devices/SPC564Axx/
    )
set(STARTUPLD ${CHIBIOS}/os/common/startup/e200/compilers/GCC/ld)

set(ALLXASMSRC ${ALLXASMSRC} ${STARTUPASM})
set(ALLINC ${ALLINC} ${STARTUPINC})
