set(STARTUPSRC ${CHIBIOS}/os/common/startup/ARM/compilers/GCC/crt1.c)
set(STARTUPASM 
    ${CHIBIOS}/os/common/startup/ARM/compilers/GCC/vectors.S
    ${CHIBIOS}/os/common/startup/ARM/compilers/GCC/crt0.S
    )
set(STARTUPINC 
    ${CHIBIOS}/os/common/portability/GCC/
    ${CHIBIOS}/os/common/startup/ARM/devices/LPC214x/
    )
set(STARTUPLD ${CHIBIOS}/os/common/startup/ARM/compilers/GCC/ld)

set(ALLXASMSRC ${ALLXASMSRC} ${STARTUPASM})
set(ALLCSRC ${ALLCSRC} ${STARTUPSRC})
set(ALLINC ${ALLINC} ${STARTUPINC})
