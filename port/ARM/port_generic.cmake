set(PORTSRC ${CHIBIOS}/os/common/ports/ARM/chcore.c)
set(PORTASM ${CHIBIOS}/os/common/ports/ARM/compilers/GCC/chcoreasm.S)
set(PORTINC
    ${CHIBIOS}/os/common/ports/ARM/
    ${CHIBIOS}/os/common/ports/ARM/compilers/GCC/
    )

set(ALLXASMSRC ${ALLXASMSRC} ${PORTASM})
set(ALLCSRC ${ALLCSRC} ${PORTSRC})
set(ALLINC ${ALLINC} ${PORTINC})
