set(PORTSRC ${CHIBIOS}/os/common/ports/SIMIA32/chcore.c)
set(PORTINC
    ${CHIBIOS}/os/common/ports/SIMIA32/compilers/GCC/
    ${CHIBIOS}/os/common/ports/SIMIA32/
    )

set(ALLCSRC ${ALLCSRC} ${PORTSRC})
set(ALLINC ${ALLINC} ${PORTINC})
