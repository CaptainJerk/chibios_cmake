set(STARTUPASM ${CHIBIOS}/os/common/startup/ARMCMx-SB/compilers/GCC/crt0.S)
set(STARTUPINC 
    ${CHIBIOS}/os/common/portability/GCC/
    ${CHIBIOS}/os/common/startup/ARMCMx-SB/compilers/GCC/
    )
set(STARTUPLD ${CHIBIOS}/os/common/startup/ARMCMx-SB/compilers/GCC/ld)

set(ALLXASMSRC ${ALLXASMSRC} ${STARTUPASM})
set(ALLINC ${ALLINC} ${STARTUPINC})
