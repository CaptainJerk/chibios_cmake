set(STARTUPSRC ${CHIBIOS}/os/common/startup/ARMCMx/compilers/GCC/crt1.c)
set(STARTUPASM
    ${CHIBIOS}/os/common/startup/ARMCMx/compilers/GCC/crt0_v6m.S
    ${CHIBIOS}/os/common/startup/ARMCMx/compilers/GCC/vectors.S
    )
set(STARTUPINC
    ${CHIBIOS}/os/common/portability/GCC/
    ${CHIBIOS}/os/common/startup/ARMCMx/compilers/GCC/
    ${CHIBIOS}/os/common/startup/ARMCMx/devices/STM32L0xx/
    ${CHIBIOS}/os/common/ext/ARM/CMSIS/Core/Include/
    ${CHIBIOS}/os/common/ext/ST/STM32L0xx/
    )
set(STARTUPLD ${CHIBIOS}/os/common/startup/ARMCMx/compilers/GCC/ld)

set(ALLXASMSRC ${ALLXASMSRC} ${STARTUPASM})
set(ALLCSRC ${ALLCSRC} ${STARTUPSRC})
set(ALLINC ${ALLINC} ${STARTUPINC})
