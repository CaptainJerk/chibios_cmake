set(OSALSRC
    ${CHIBIOS}/os/hal/osal/os-less/ARMCMx/osal.c
    ${CHIBIOS}/os/hal/osal/lib/osal_vt.c
    )
set(OSALINC
    ${CHIBIOS}/os/hal/osal/os-less/ARMCMx/
    ${CHIBIOS}/os/hal/osal/lib/
    )

set(ALLCSRC ${ALLCSRC} ${OSALSRC})
set(ALLINC ${ALLINC} ${OSALINC})
