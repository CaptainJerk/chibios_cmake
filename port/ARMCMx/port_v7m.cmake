set(PORTSRC
    ${CHIBIOS}/os/common/ports/ARMCMx/chcore.c
    ${CHIBIOS}/os/common/ports/ARMCMx/chcore_v7m.c
    )
set(PORTASM ${CHIBIOS}/os/common/ports/ARMCMx/compilers/GCC/chcoreasm_v7m.S)
set(PORTINC
    ${CHIBIOS}/os/common/ports/ARMCMx/
    ${CHIBIOS}/os/common/ports/ARMCMx/compilers/GCC/
    )

set(ALLXASMSRC ${ALLXASMSRC} ${PORTASM})
set(ALLCSRC ${ALLCSRC} ${PORTSRC})
set(ALLINC ${ALLINC} ${PORTINC})
