set(PORTSRC ${CHIBIOS}/os/common/ports/AVR/chcore.c)
set(PORTINC
    ${CHIBIOS}/os/common/ports/AVR/
    ${CHIBIOS}/os/common/ports/AVR/compilers/GCC/
    )

set(ALLCSRC ${ALLCSRC} ${PORTSRC})
set(ALLINC ${ALLINC} ${PORTINC})
