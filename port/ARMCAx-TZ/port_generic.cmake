set(PORTSRC
    ${CHIBIOS}/os/common/ports/ARMCAx-TZ/chcore.c
    ${CHIBIOS}/os/common/ports/ARMCAx-TZ/chtssi.c
    )
set(PORTASM
    ${CHIBIOS}/os/common/ports/ARMCAx-TZ/compilers/GCC/chcoreasm.S
    ${CHIBIOS}/os/common/ports/ARMCAx-TZ/compilers/GCC/monitor.S
    )
set(PORTINC
    ${CHIBIOS}/os/common/ports/ARMCAx-TZ/
    ${CHIBIOS}/os/common/ports/ARMCAx-TZ/compilers/GCC/
    )

set(ALLXASMSRC ${ALLXASMSRC} ${PORTASM})
set(ALLCSRC ${ALLCSRC} ${PORTSRC})
set(ALLINC ${ALLINC} ${PORTINC})
