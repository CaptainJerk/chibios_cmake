set(NILSRC
    ${CHIBIOS}/os/nil/src/ch.c
    ${CHIBIOS}/os/nil/src/chevt.c
    ${CHIBIOS}/os/nil/src/chmsg.c
    ${CHIBIOS}/os/nil/src/chsem.c
    ${CHIBIOS}/os/oslib/src/chmboxes.c 
    ${CHIBIOS}/os/oslib/src/chmemcore.c 
    ${CHIBIOS}/os/oslib/src/chmemheaps.c 
    ${CHIBIOS}/os/oslib/src/chmempools.c 
    ${CHIBIOS}/os/oslib/src/chpipes.c 
    ${CHIBIOS}/os/oslib/src/chobjcaches.c 
    ${CHIBIOS}/os/oslib/src/chdelegates.c 
    ${CHIBIOS}/os/oslib/src/chfactory.c
    )
set(NILINC 
    ${CHIBIOS}/os/nil/include/
    ${CHIBIOS}/os/oslib/include/
    )
set(OSALSRC ${CHIBIOS}/os/hal/osal/rt-nil/osal.c)
set(OSALINC ${CHIBIOS}/os/hal/osal/rt-nil)

set(ALLCSRC ${ALLCSRC} ${NILSRC} ${OSALSRC})
set(ALLINC ${ALLINC} ${NILINC} ${OSALINC})
set(ALLDOCS 
    ${ALLDOCS}
    ${CHIBIOS}/doc/nil/src/
    ${CHIBIOS}/os/nil/dox/
    ${CHIBIOS}/os/nil/src/
    ${CHIBIOS}/os/nil/include/
    ${CHIBIOS}/os/oslib/dox/
    ${CHIBIOS}/os/oslib/src/
    ${CHIBIOS}/os/oslib/include/
    ${CHIBIOS}/os/various/
    ${CHIBIOS}/os/common/abstractions/cmsis_os/
    )
