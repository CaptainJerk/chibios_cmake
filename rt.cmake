set(RTSRC
    ${CHIBIOS}/os/rt/src/chsys.c 
    ${CHIBIOS}/os/rt/src/chdebug.c 
    ${CHIBIOS}/os/rt/src/chtrace.c 
    ${CHIBIOS}/os/rt/src/chvt.c 
    ${CHIBIOS}/os/rt/src/chschd.c 
    ${CHIBIOS}/os/rt/src/chthreads.c 
    ${CHIBIOS}/os/rt/src/chtm.c 
    ${CHIBIOS}/os/rt/src/chstats.c 
    ${CHIBIOS}/os/rt/src/chregistry.c 
    ${CHIBIOS}/os/rt/src/chsem.c 
    ${CHIBIOS}/os/rt/src/chmtx.c 
    ${CHIBIOS}/os/rt/src/chcond.c 
    ${CHIBIOS}/os/rt/src/chevents.c 
    ${CHIBIOS}/os/rt/src/chmsg.c 
    ${CHIBIOS}/os/rt/src/chdynamic.c
    ${CHIBIOS}/os/oslib/src/chmboxes.c 
    ${CHIBIOS}/os/oslib/src/chmemcore.c 
    ${CHIBIOS}/os/oslib/src/chmemheaps.c 
    ${CHIBIOS}/os/oslib/src/chmempools.c 
    ${CHIBIOS}/os/oslib/src/chpipes.c 
    ${CHIBIOS}/os/oslib/src/chobjcaches.c 
    ${CHIBIOS}/os/oslib/src/chdelegates.c 
    ${CHIBIOS}/os/oslib/src/chfactory.c
    ${CHIBIOS}/os/common/abstractions/cmsis_os/cmsis_os.c
    )
set(RTINC 
    ${CHIBIOS}/os/rt/include/
    ${CHIBIOS}/os/oslib/include/
    ${CHIBIOS}/os/common/abstractions/cmsis_os/
    )
set(OSALSRC ${CHIBIOS}/os/hal/osal/rt-nil/osal.c)
set(OSALINC ${CHIBIOS}/os/hal/osal/rt-nil)

set(ALLCSRC ${ALLCSRC} ${RTSRC} ${OSALSRC})
set(ALLINC ${ALLINC} ${RTINC} ${OSALINC})
set(ALLDOCS 
    ${ALLDOCS}
    ${CHIBIOS}/doc/rt/src/
    ${CHIBIOS}/os/rt/dox/
    ${CHIBIOS}/os/rt/src/
    ${CHIBIOS}/os/rt/include/
    ${CHIBIOS}/os/oslib/dox/
    ${CHIBIOS}/os/oslib/src/
    ${CHIBIOS}/os/oslib/include/
    ${CHIBIOS}/os/various/
    ${CHIBIOS}/os/common/abstractions/cmsis_os/
    )

