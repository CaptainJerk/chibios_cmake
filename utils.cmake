function(add_bsp_target targ)
    cmake_parse_arguments(PA 
        ""
        ""
        "PUBLIC_CFLAGS;PRIVATE_CFLAGS;INTERFACE_CFLAGS;PUBLIC_LDFLAGS;PRIVATE_LDFLAGS;INTERFACE_LDFLAGS;SOURCE;DOCS"
        ${ARGN}
        )

    foreach(I IN LISTS ALLINC)
        set(INCS ${INCS} $<BUILD_INTERFACE:${I}>)
    endforeach()

    find_package(Doxygen REQUIRED dot)
    if(DOXYGEN_FOUND)
        set(DOXYGEN_PROJECT_NAME "ChibiOS ${targ}")
        doxygen_add_docs(docs ${ALLDOCS} ${PA_DOCS} ${PA_SOURCE})
    endif()

    add_library(${targ} STATIC 
        ${ALLCSRC} ${ALLXASMSRC} ${ALLASMSRC} 
        ${PA_SOURCE}
        )
    target_compile_options(${targ} 
        PUBLIC ${PA_PUBLIC_CFLAGS}
        PRIVATE ${PA_PRIVATE_CFLAGS}
        INTERFACE ${PA_INTERFACE_CFLAGS}
        )
    target_link_options(${targ} 
        PUBLIC ${PA_PUBLIC_LDFLAGS}
        PRIVATE ${PA_PRIVATE_LDFLAGS}
        INTERFACE ${PA_INTERFACE_LDFLAGS}
        )
    target_link_directories(${targ}
        INTERFACE $<INSTALL_PREFIX>/lib/ $<INSTALL_PREFIX>/ld/${targ}/
        )
    target_include_directories(${targ} PUBLIC ${INCS})
endfunction()

function(install_bsp_target targ)
    get_target_property(INCS ${targ} INTERFACE_INCLUDE_DIRECTORIES)
    foreach(I IN LISTS INCS)
        string(REPLACE "$<BUILD_INTERFACE:" "" I ${I})
        string(REPLACE ">" "" I ${I})
        file(GLOB _INC_FILES ${I}/*.h)
        set(INC_FILES ${INC_FILES} ${_INC_FILES})
    endforeach()

    file(GLOB LDRULES ${STARTUPLD}/rule*.ld)
    install(TARGETS ${targ}
        EXPORT "${targ}-config"
        LIBRARY DESTINATION "lib"
        INCLUDES DESTINATION "include/${targ}"
        )
    install(DIRECTORY 
        ${CMAKE_CURRENT_BINARY_DIR}/html/ 
        DESTINATION "doc/${targ}"
        OPTIONAL
        )
    install(FILES ${LDRULES} DESTINATION "ld/${targ}")
    install(FILES ${INC_FILES} DESTINATION "include/${targ}")
    install(EXPORT "${targ}-config" DESTINATION "cmake")
endfunction()

function(package_bsp_target targ)
    set(CPACK_PACKAGE_NAME ${targ})
    set(CPACK_PACKAGE_VERSION ${PROJECT_VERSION})
    set(CPACK_PACKAGE_FILE_NAME ${targ}-${CPACK_PACKAGE_VERSION})
    set(CPACK_GENERATOR "ZIP")
    include(CPack)
endfunction()

macro(reset_scope)
    set(ALLCSRC "")
    set(ALLXASMSRC "")
    set(ALLASMSRC "")
    set(ALLINC "")
    set(ALLDOCS "")
endmacro()
