set(EXSRC 
    ${CHIBIOS}/os/ex/devices/Bosch/bmp085.c
    ${CHIBIOS}/os/ex/devices/ST/hts221.c
    ${CHIBIOS}/os/ex/devices/ST/l3gd20.c
    ${CHIBIOS}/os/ex/devices/ST/lis302dl.c
    ${CHIBIOS}/os/ex/devices/ST/lis3dsh.c
    ${CHIBIOS}/os/ex/devices/ST/lis3mdl.c
    ${CHIBIOS}/os/ex/devices/ST/lps22hb.c
    ${CHIBIOS}/os/ex/devices/ST/lps25h.c
    ${CHIBIOS}/os/ex/devices/ST/lsm303agr.c
    ${CHIBIOS}/os/ex/devices/ST/lsm303dlhc.c
    ${CHIBIOS}/os/ex/devices/ST/lsm6ds0.c
    ${CHIBIOS}/os/ex/devices/ST/lsm6dsl.c
    )
set(EXINC
    ${CHIBIOS}/os/ex/include/
    ${CHIBIOS}/os/ex/devices/Bosch/
    ${CHIBIOS}/os/ex/devices/ST/
    )

set(ALLCSRC ${ALLCSRC} ${EXSRC})
set(ALLINC ${ALLINC} ${EXINC})
set(ALLDOCS 
    ${ALLDOCS}
    ${CHIBIOS}/os/ex/dox/
    ${CHIBIOS}/os/ex/include/
    ${CHIBIOS}/os/ex/devices/ST/
    ${CHIBIOS}/os/ex/devices/Bosch/
    )
