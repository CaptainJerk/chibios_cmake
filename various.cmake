set(VARSRC
    ${CHIBIOS}/os/various/evtimer.c
    ${CHIBIOS}/os/various/syscalls.c
    )
set(VARINC ${CHIBIOS}/os/various/)

set(ALLCSRC ${ALLCSRC} ${VARSRC})
set(ALLINC ${ALLINC} ${VARINC})
