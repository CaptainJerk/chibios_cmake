set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/simulator/win32/hal_lld.c
    ${CHIBIOS}/os/hal/ports/simulator/win32/hal_serial_lld.c
    ${CHIBIOS}/os/hal/ports/simulator/console.c
    ${CHIBIOS}/os/hal/ports/simulator/hal_pal_lld.c
    ${CHIBIOS}/os/hal/ports/simulator/hal_st_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/simulator/win32/
    ${CHIBIOS}/os/hal/ports/simulator/
    )

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
