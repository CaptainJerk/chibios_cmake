set(PLATFORMSRC ${CHIBIOS}/os/hal/ports/AVR/MEGA/ATMEGAxx/hal_lld.c)
set(PLATFORMINC ${CHIBIOS}/os/hal/ports/AVR/MEGA/ATMEGAxx/)

include(${CMAKE_CURRENT_LIST_DIR}/drivers/ADCv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/EXTv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/GPIOv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/I2Cv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/SPIv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/TIMv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USARTv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USBv1.cmake)

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
