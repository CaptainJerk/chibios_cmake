set(PLATFORMSRC ${CHIBIOS}/os/hal/ports/AVR/XMEGA/ATXMEGAxxxA4U/hal_lld.c)
set(PLATFORMINC ${CHIBIOS}/os/hal/ports/AVR/XMEGA/ATXMEGAxxxA4U/)

include(${CMAKE_CURRENT_LIST_DIR}/drivers/CRYPv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DACv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DMAv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/GPIOv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/SPIv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/TIMv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USARTv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/WDGv1.cmake)

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
