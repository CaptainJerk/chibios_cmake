set(PLATFORMSRC ${CHIBIOS}/os/hal/ports/AVR/TINY/ATTinyxxx/hal_lld.c)
set(PLATFORMINC ${CHIBIOS}/os/hal/ports/AVR/TINY/ATTinyxxx/)

include(${CMAKE_CURRENT_LIST_DIR}/drivers/GPIOv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/TIMv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USARTv1.cmake)

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
