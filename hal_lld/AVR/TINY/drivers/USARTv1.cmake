set(PLATFORMSRC
    ${PLATFORMSRC}
    ${CHIBIOS}/os/hal/ports/AVR/TINY/LLD/USARTv1/hal_serial_lld.c
    ${CHIBIOS}/os/hal/ports/AVR/TINY/LLD/USARTv1/hal_uart_lld.c
    )

set(PLATFORMINC ${PLATFORMINC} ${CHIBIOS}/os/hal/ports/AVR/TINY/LLD/USARTv1/)
