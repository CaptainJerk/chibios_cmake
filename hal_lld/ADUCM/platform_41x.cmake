set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/nvic.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM41x/aducm_isr.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM41x/hal_lld.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM41x/hal_st_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM41x/
    )

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
