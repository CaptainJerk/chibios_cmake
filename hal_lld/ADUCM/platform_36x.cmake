set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/nvic.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM36x/aducm_isr.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM36x/hal_lld.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM36x/hal_pal_lld.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM36x/hal_serial_lld.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM36x/hal_spi_lld.c
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM36x/hal_st_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/
    ${CHIBIOS}/os/hal/ports/ADUCM/ADUCM36x/
    )

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
