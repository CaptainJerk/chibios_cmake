set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/nvic.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32H7xx/stm32_isr.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32H7xx/hal_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/
    ${CHIBIOS}/os/hal/ports/STM32/STM32H7xx/
    )

include(${CMAKE_CURRENT_LIST_DIR}/drivers/ADCv4.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/BDMAv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/CRYPv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DACv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DMAv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/EXTIv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/FDCANv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/GPIOv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/I2Cv3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/MDMAv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/OTGv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/QUADSPIv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/SDMMCv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/SPIv3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/RNGv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/RTCv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/TIMv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USARTv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/xWDGv1.cmake)

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
