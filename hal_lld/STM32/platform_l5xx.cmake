set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/nvic.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32L5xx/stm32_isr.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32L5xx/hal_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/
    ${CHIBIOS}/os/hal/ports/STM32/STM32L5xx/
    )

include(${CMAKE_CURRENT_LIST_DIR}/drivers/GPIOv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USARTv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/xWDGv1.cmake)

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
