set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/nvic.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32L4xx+/stm32_isr.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32L4xx+/hal_lld.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32L4xx+/hal_efl_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/
    ${CHIBIOS}/os/hal/ports/STM32/STM32L4xx+/
    )

include(${CMAKE_CURRENT_LIST_DIR}/drivers/ADCv3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/CANv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/CRYPv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DACv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DMAv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/EXTIv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/GPIOv3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/I2Cv3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/OCTOSPIv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/RNGv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/RTCv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/SDMMCv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/SPIv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/TIMv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USARTv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/OTGv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/xWDGv1.cmake)

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
