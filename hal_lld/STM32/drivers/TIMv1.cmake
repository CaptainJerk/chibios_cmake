set(PLATFORMSRC
    ${PLATFORMSRC}
    ${CHIBIOS}/os/hal/ports/STM32/LLD/TIMv1/hal_st_lld.c
    ${CHIBIOS}/os/hal/ports/STM32/LLD/TIMv1/hal_gpt_lld.c
    ${CHIBIOS}/os/hal/ports/STM32/LLD/TIMv1/hal_icu_lld.c
    ${CHIBIOS}/os/hal/ports/STM32/LLD/TIMv1/hal_pwm_lld.c
    )
set(PLATFORMINC ${PLATFORMINC} ${CHIBIOS}/os/hal/ports/STM32/LLD/TIMv1/)
