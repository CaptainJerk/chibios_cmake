set(PLATFORMSRC
    ${PLATFORMSRC}
    ${CHIBIOS}/os/hal/ports/STM32/LLD/SPIv2/hal_i2s_lld.c
    ${CHIBIOS}/os/hal/ports/STM32/LLD/SPIv2/hal_spi_lld.c
    )
set(PLATFORMINC ${PLATFORMINC} ${CHIBIOS}/os/hal/ports/STM32/LLD/SPIv2/)
