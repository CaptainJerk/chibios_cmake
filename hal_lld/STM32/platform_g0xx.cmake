set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/nvic.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32G0xx/stm32_isr.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32G0xx/hal_lld.c
    ${CHIBIOS}/os/hal/ports/STM32/STM32G0xx/hal_efl_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/common/ARMCMx/
    ${CHIBIOS}/os/hal/ports/STM32/STM32G0xx/
    )

include(${CMAKE_CURRENT_LIST_DIR}/drivers/ADCv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DACv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/DMAv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/EXTIv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/GPIOv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/I2Cv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/RNGv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/RTCv3.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/SPIv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/TIMv1.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/USARTv2.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/drivers/xWDGv1.cmake)

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
