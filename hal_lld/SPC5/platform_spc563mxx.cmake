set(PLATFORMSRC
    ${CHIBIOS}/os/hal/ports/SPC5/SPC563Mxx/hal_lld.c
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/DSPI_v1/hal_spi_lld.c
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/EDMA_v1/spc5_edma.c
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/ESCI_v1/hal_serial_lld.c
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/SIU_v1/hal_pal_lld.c
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/STM_v1/hal_st_lld.c
    )
set(PLATFORMINC
    ${CHIBIOS}/os/hal/ports/SPC5/SPC563Mxx/
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/DSPI_v1/
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/EDMA_v1/
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/ESCI_v1/
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/SIU_v1/
    ${CHIBIOS}/os/hal/ports/SPC5/LLD/STM_v1/
    )

set(ALLCSRC ${ALLCSRC} ${PLATFORMSRC})
set(ALLINC ${ALLINC} ${PLATFORMINC})
